import { Router } from 'express';
import { getCategorias, getCategoria, createCategoria, updateCategoria, deleteCategoria } from '../controllers/categoria.controller.js';
import { verifyToken } from '../middleware/login.js';


const router = Router();

// Routes
router.get('/', verifyToken, getCategorias);

router.post('/', verifyToken, createCategoria);

router.get('/:id', verifyToken, getCategoria);

router.put('/:id', verifyToken, updateCategoria);

router.delete('/:id', verifyToken, deleteCategoria);

export default router;