import { Router } from 'express';
import { getProductos, getProducto, createProducto, updateProducto, deleteProducto } from '../controllers/producto.controller.js';
import { verifyToken } from '../middleware/login.js';

const router = Router();

// Routes
router.get('/', verifyToken, getProductos);

router.post('/', verifyToken, createProducto);

router.get('/:id', verifyToken, getProducto);

router.put('/:id', verifyToken, updateProducto);

router.delete('/:id', verifyToken, deleteProducto);

export default router;