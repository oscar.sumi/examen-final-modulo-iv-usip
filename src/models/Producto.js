import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';

export const Producto = sequelize.define(
    'productos', 
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Nombre del Producto',
        },
        precio_unitario: {
            type: DataTypes.DECIMAL(18, 2),
            allowNull: false,
            comment: 'Precio unitario del producto',
        },
        estado: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        }
});