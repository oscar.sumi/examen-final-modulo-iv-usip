import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';
import { Categoria } from './Categoria.js';
import { Producto } from './Producto.js';

export const Usuario = sequelize.define(
    'usuarios',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        nombre: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Nombre de usuario',
        },
        correo: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Correo electrónico del usuario',
        },
        contrasena: {
            type: DataTypes.STRING,
            allowNull: false,
            comment: 'Contraseña del usuario',
        },
        estado: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            comment: 'Estado de la cuenta de usuario'
        },
    }
);

Usuario.hasMany(Categoria, {
    foreignKey: 'usuario_id',
    sourceKey: 'id',
});

Categoria.belongsTo(Usuario, {
    foreignKey: 'usuario_id',
    targetKey: 'id',
});

Usuario.hasMany(Producto, {
    foreignKey: 'usuario_id',
    sourceKey: 'id',
});

Producto.belongsTo(Usuario, {
    foreignKey: 'usuario_id',
    targetKey: 'id',
});